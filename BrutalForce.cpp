#include "InstanceBrutal.h"
#include "timeMeasure.h"

void BrutalDFS(InstanceBrutal *batoh, vector<bool> config, uint16_t fixovaneBity, uint32_t aktualniVaha, uint32_t aktualniCena){
    batoh->mereni++;

    if( batoh->Nadlimitni(aktualniVaha) ) return;
    if( batoh->Splnuje(aktualniVaha, aktualniCena)){
        if( batoh->nejlepsiCena < aktualniCena){
            batoh->config = config;
            batoh->nejlepsiCena = aktualniCena;

            if(batoh->nejlepsiCena >= batoh->pozadovanaCena && batoh->rozhodovaci) throw batoh->mereni;
        }
    }
    if( fixovaneBity == batoh->pocetVeci ) return;
    if( batoh->BB && !batoh->UzitecnaCesta(aktualniCena, fixovaneBity) ) return; //B&B

    for (uint16_t i = fixovaneBity; i < batoh->pocetVeci; i++){
        config[ i ] = true;
        BrutalDFS(batoh, config, i + (uint16_t) 1, (aktualniVaha + batoh->Itemy[i].first),
                  (aktualniCena + batoh->Itemy[i].second));
        config[ i ] = false;
    }
}


/**
 * Vyřeší jednu instanci s n položkami a s ID
 * vrací počet navstivenych conf
 */
uint64_t runBrutal(uint8_t n, int16_t ID, bool vypis, bool rozhodovaci, bool BB){

    //inicializace zadani batohu
    InstanceBrutal instance = InstanceBrutal(n, ID, rozhodovaci, BB);

    //zpracování instance
    if(vypis) cout << fabs(ID) << " " << instance.pocetVeci << " ";

    //mereni casu
    timeStart();

    //rozhodovaci řeším catchem, abych nemusel předelávat konstruktivní verzi
    if(rozhodovaci){
        try{
            BrutalDFS(&instance, vector<bool>(instance.pocetVeci, false), 0, 0, 0);
        }catch(uint64_t c){

        }
    }else{
        BrutalDFS(&instance, vector<bool>(instance.pocetVeci, false), 0, 0, 0);
    }
    timeStop();

    if(vypis) {
        instance.vypis();
    }

    if(POCET_KONFIGURACI) return instance.mereni;
    else return timeGet();

}
