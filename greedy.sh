#!/usr/bin/env bash

g++ -std=c++14 -Wall -o program main.cpp InstanceBrutal.cpp DynamicInstance.cpp GreedyInstance.cpp Instance.cpp

tmp=/tmp/mereni
tmp2=/tmp/mereni2
rm ./report/heuristika/g* ./report/heuristika/r* ./report/heuristika/cas_*

#sadaDat="ZKC_unique/ZKC"
sadaDat="NR/NK"

for mod in "greedy" "redux"
do

  for N in 4 10 15 20 22 25 27 30 32 35 37 40
  do

    ./program heuristika $mod $N >$tmp 2>>./report/heuristika/cas_${mod}
    paste $tmp ${sadaDat}${N}_sol.dat | awk '{print $1 "," $4 "," $4-$1}' >>./report/heuristika/${mod}

    if test $mod == "redux"
    then
         paste $tmp ${sadaDat}${N}_sol.dat | awk '$4!=0{ if ( (($4-$1)/$4) > 0.5 ) { print "horsi chyba nez 0.5 u '$mod': " (($4-$1)/$4) " " $0 } }'
         paste $tmp ${sadaDat}${N}_sol.dat | awk '$4<$1{ print "mensi " $0 }'
    fi

  done
done

if false
then

  rm ./report/heuristika/f* ./report/heuristika/casy

  for epsilon in {1..100}
  do

    echo "provadi se: " $epsilon

    for N in 4 10 15 20 22 25 27 30 32 35 37 40
    do


      ./program fptas $epsilon $N >$tmp 2>$tmp2

      #kontrola
      paste $tmp NR/NK${N}_sol.dat | awk '$4!=0{ if (100*$1/$4 < 100-'$epsilon') { print "epsilon " $1/$4 " " $0 } }'
      paste $tmp NR/NK${N}_sol.dat | awk '$1>$4{print "lepsi " $0}'

      #statistika
      paste $tmp NR/NK${N}_sol.dat | awk '{print $1 "," $4 "," $4-$1}' >./report/heuristika/fptas_${epsilon}_${N}
      awk '{s+=$1} END {print s "/(500*1000000)"}' $tmp2 | bc -l | tr -d '\n' >>./report/heuristika/casy

      if test $N -ne 40
      then
        echo -n "," >>./report/heuristika/casy
      fi


    done

      echo >> ./report/heuristika/casy
  done


fi