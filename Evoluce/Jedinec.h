//
// Created by davidpokzgmail.com on 3/13/2019.
//

#ifndef GENETICKY_JEDINEC_H
#define GENETICKY_JEDINEC_H


#include <ostream>

class Jedinec {
private:
    double precomputedFitnes = 0;
    bool isFitnessPrecomputed = false;

    virtual double CountFitness()  = 0;

public:
    Jedinec() = default;
    virtual ~Jedinec() = default;

    double Fitness();
    virtual Jedinec * Mutace() = 0;
    virtual Jedinec *Krizeni(Jedinec *parent) = 0;


    friend bool operator<( Jedinec &lhs,  Jedinec &rhs);
    friend bool operator>( Jedinec &lhs,  Jedinec &rhs);
    friend bool operator<=( Jedinec &lhs,  Jedinec &rhs);
    friend bool operator>=( Jedinec &lhs,  Jedinec &rhs);

    static bool comparePtrToJedinec(Jedinec* a, Jedinec* b);

    virtual void Print() = 0;

};



#endif //GENETICKY_JEDINEC_H
