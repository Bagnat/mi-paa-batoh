//
// Created by davidpokzgmail.com on 3/13/2019.
//

#ifndef GENETICKY_EVOLUCE_H
#define GENETICKY_EVOLUCE_H


#include <vector>
#include <cstdlib>
#include <algorithm>
#include <cstdint>
#include <iostream>
#include <cmath>
#include <random>
#include "Jedinec.h"
#include "Konstanty.h"
#include "../Instance.h"
#include "JedinecBatoh.h"


using namespace std;


template <class JEDINEC_CLASS>
class Evoluce {
private:

    vector<Jedinec*> populace;
    vector<Jedinec*> novaPopulace;

    Jedinec* Turnaj();
    Jedinec* Turnaj(Jedinec* vynech);
    Instance* instance;

public:
    /**
     * Vytvoří populaci
     */
    Evoluce( Instance* inst );

    void Prirustek(uint32_t pocet); // přidá počet náhodných nových jedinců

    void Mutuj();

    void Kriz();

    void Reprodukce();

    void Koncentrate(); //vyber jedincu pro další rundu

    void SmazStejne();

    double Zij();

    /**
     * vyhladí populaci a zanechá jen nejlepšího
     */
    void Katastrofa();

    void PrintHistogramFitness();

    void Sort();

    void PrintStatus();
    void PrintPopulation();

};

template class Evoluce<JedinecBatoh>;

#endif //GENETICKY_EVOLUCE_H
