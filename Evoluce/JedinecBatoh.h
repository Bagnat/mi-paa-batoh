//
// Created by Bagnat on 15.12.2019.
//

#ifndef BATOH_JEDINECBATOH_H
#define BATOH_JEDINECBATOH_H


#include <vector>
#include "Jedinec.h"
#include "../Instance.h"

using namespace std;

class JedinecBatoh : public Jedinec {
private:
    vector<bool> konfigurace;
    Instance * instance;

    double CountFitness() override;
public:
    Jedinec *Mutace() override;

    void Print() override;

    explicit JedinecBatoh(Instance * instance);
    JedinecBatoh(Instance * instance, vector<bool> konfigurace);

    Jedinec *Krizeni(Jedinec *parent) override;

};


#endif //BATOH_JEDINECBATOH_H
