//
// Created by davidpokzgmail.com on 3/13/2019.
//

#include "Jedinec.h"

bool operator<( Jedinec &lhs,  Jedinec &rhs) {
    return lhs.Fitness() < rhs.Fitness();
}

bool operator>( Jedinec &lhs,  Jedinec &rhs) {
    return rhs < lhs;
}

bool operator<=( Jedinec &lhs,  Jedinec &rhs) {
    return !(rhs < lhs);
}

bool operator>=( Jedinec &lhs,  Jedinec &rhs) {
    return !(lhs < rhs);
}

bool Jedinec::comparePtrToJedinec(Jedinec *a, Jedinec *b) { return (*a < *b); }

double Jedinec::Fitness() {
    if(isFitnessPrecomputed) return precomputedFitnes;
    precomputedFitnes = CountFitness();
    isFitnessPrecomputed = true;
    return precomputedFitnes;
}


std::ostream &operator<<(std::ostream &os,  Jedinec &jedinec) {
    os << "neni def." << std::endl;
    return os;
}



