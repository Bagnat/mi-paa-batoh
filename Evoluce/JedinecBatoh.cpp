//
// Created by Bagnat on 15.12.2019.
//

#include "JedinecBatoh.h"
#include "Konstanty.h"

JedinecBatoh::JedinecBatoh(Instance * instance) : instance(instance) {
    konfigurace = vector<bool>(instance->pocetVeci, false);
    for (int i = 0; i < instance->pocetVeci; ++i) {
        if(rand()%1000 < PST_PRIDANI_ITEMU_PRI_GENEROVANI){
            konfigurace[i] = true;
        }
    }
}

double JedinecBatoh::CountFitness() {
    double vaha = 0, cena = 0;
    for (int i = 0; i < instance->pocetVeci; ++i) {
        if(konfigurace[i]){
            vaha += instance->Itemy[i].first;
            cena += instance->Itemy[i].second;
        }
    }

    if(vaha > instance->maximalniKapacita) {
        if(cena > instance->nejlepsiCena && rand()%1000 < PST_UDELENI_FITNESS_PRI_NESPLNENI_VAHY){
            return -cena * instance->maximalniKapacita / vaha;
        } else return 0;
    };

    //uložení pokud je nejlepší
    if(instance->nejlepsiCena < cena){
        instance->nejlepsiCena = cena;
        instance->config = konfigurace;
    }

    return -cena;
}

Jedinec *JedinecBatoh::Mutace() {
    auto novaKonfigurace = konfigurace;

    for (int i = 0; i < instance->pocetVeci; ++i) {
        if(rand()%1000 < PST_MUTACE_JEDNOHO_ITEMU){
            novaKonfigurace[i] = !novaKonfigurace[i];
        }
    }

    return new JedinecBatoh(instance, novaKonfigurace);
}

Jedinec *JedinecBatoh::Krizeni(Jedinec *parent) {
    auto novaKonfigurace = vector<bool>(instance->pocetVeci, false);
    uint rozdel = rand()%instance->pocetVeci;

    for (int i = 0; i < rozdel; ++i) {
            novaKonfigurace[i] = this->konfigurace[i];
    }

    for (int i = rozdel; i < instance->pocetVeci; ++i) {
            novaKonfigurace[i] = ((JedinecBatoh*)parent)->konfigurace[i];
    }

    return new JedinecBatoh(instance, novaKonfigurace);
}

JedinecBatoh::JedinecBatoh(Instance *instance, vector<bool> konfigurace) : konfigurace(konfigurace), instance(instance) {

}

void JedinecBatoh::Print() {

}

