//
// Created by davidpokzgmail.com on 3/13/2019.
//

#include "Evoluce.h"
#include "../Instance.h"


template<class JEDINEC_CLASS>
Evoluce<JEDINEC_CLASS>::Evoluce(Instance* instance) : instance(instance) {
    Prirustek(VELIKOST_POPULACE);
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Prirustek(uint32_t pocet) {
    for (int i = 0; i < pocet; ++i) {
        Jedinec* jedinec = new JEDINEC_CLASS(instance);
        populace.push_back( jedinec );
    }
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Sort() {
    sort(populace.begin(), populace.end(), Jedinec::comparePtrToJedinec);
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::PrintHistogramFitness() {
    if(!VYPIS_HISTOGRAM) return;
    Sort();
    uint pocty[HISTOGRAM_POCET_SLOUPCU] = {0};
    int min = (int)populace[0]->Fitness(), max = (int)populace[ round(populace.size()*0.75) ]->Fitness();
    int sirka = abs(min-max) / HISTOGRAM_POCET_SLOUPCU;
    int cnt = 0, point = 0;
    //cnt je cislo sloupce, i je hodnota, point je misto v pole, kde to kontroluji
    for (int i = min+sirka; i < max; i+=sirka, ++cnt) {
        while (point < populace.size() && populace[point]->Fitness() < i){
            pocty[cnt]++;
            point++;
        }
    }

    cout << "Histogram: ";
    for (int j = 0; j < cnt; ++j) {
        cout << pocty[j] << " ";
    }
    cout << "Sirka: " << sirka << "\tRozsah: " << min << "-" << max;
    cout << endl << endl;

}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Katastrofa() {
    novaPopulace.push_back(populace[0]);
    populace.erase(populace.begin());
    Koncentrate();
    Prirustek(VELIKOST_POPULACE);
}

template<class JEDINEC_CLASS>
double Evoluce<JEDINEC_CLASS>::Zij() {
    Kriz();
    Mutuj();
    Reprodukce();
    Koncentrate();
    Prirustek(POCET_PRIRUSTKU);
    //SmazStejne();
    return populace[0]->Fitness();
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::SmazStejne() {
    auto last = populace.begin();
    for (auto i = last+1; i < populace.end(); ++i) {
        if( (*last)->Fitness() != (*i)->Fitness() || rand()%5!=0 ){
            //prezije
        }else{
            //neprezije
            populace.erase(last);
            i--;
        }
        last = i;
    }
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Koncentrate() {
    /*Sort();

    std::default_random_engine generator;
    double sigma = populace[ (int)min((int)populace.size(), VELIKOST_POPULACE) ]->Fitness() * UPRAVA_SIGMY_ODEBRANI;
    std::normal_distribution<double> distribution( 0 , sigma);
    double posun = populace[0]->Fitness();


    for (auto i = populace.begin(); i < populace.end() ; i++) {
        double number = distribution(generator);
        if(fabs(number) < (*i)->Fitness()-posun){
            populace.erase(i);
            i--;
        }
    }*/

    //chci zanechat nejlepšího
    novaPopulace.push_back(populace[0]);
    populace.erase(populace.begin());


    for (int i = 0; i < populace.size(); ++i) {
        delete(populace[i]);
    }
    populace = novaPopulace;
    novaPopulace.clear();
}

template<class JEDINEC_CLASS>
Jedinec *Evoluce<JEDINEC_CLASS>::Turnaj(Jedinec *vynech) {
    Jedinec* best = populace[ rand()%populace.size() ];

    for (int i = 0; i < POCET_V_TURNAJI; ++i) {
        Jedinec* tmp = populace[ rand()%populace.size() ];
        if(vynech != tmp && *best > *tmp) best = tmp;
    }

    if(best == vynech) return Turnaj(vynech);
    else return best;
}

template<class JEDINEC_CLASS>
Jedinec *Evoluce<JEDINEC_CLASS>::Turnaj() {
    return Turnaj(nullptr);
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Mutuj() {
    int max = (int) populace.size();
    for (int i = 0; i < max; ++i) {
        if(rand()%100 < PST_MUTACE) novaPopulace.push_back( populace[i]->Mutace() );
    }
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Kriz() {
    Jedinec *a, *b, *y;
    for (int i = 0; i < POCET_KRIZENI; ++i) {
        a = Turnaj();
        b = Turnaj(a);
        y = a->Krizeni(b);
        novaPopulace.push_back( y );
    }
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::PrintStatus() {
    cout << "Velikost populace: " << populace.size() << endl;
}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::Reprodukce() {
    for (int i = 0; i < POCET_REPRODUKCE; ++i) {
        JEDINEC_CLASS *repro = new JEDINEC_CLASS( instance );
        JEDINEC_CLASS *parent = (JEDINEC_CLASS*) Turnaj();
        (*repro) = *(parent);
        novaPopulace.push_back( repro );
    }

}

template<class JEDINEC_CLASS>
void Evoluce<JEDINEC_CLASS>::PrintPopulation() {
    cout << "{";
    for (int i = 0; i < populace.size(); ++i) {
        cout << populace[i]->Fitness();
        if( i+1 != populace.size() ) cout << ",";
    }
    cout << "}" << endl;
}
