rm cachegrind.out.* program
g++ -std=c++14 -g -Wall -o program main.cpp InstanceBrutal.cpp DynamicInstance.cpp Instance.cpp
valgrind --tool=cachegrind $PWD/program dynamicka valgrind 15
cg_annotate $PWD/cachegrind.out.* $PWD/DynamicInstance.cpp
