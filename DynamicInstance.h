//
// Created by Bagnat on 18.10.2019.
//

#ifndef BATOH_DYNAMICINSTANCE_H
#define BATOH_DYNAMICINSTANCE_H


#include <cstdint>
#include <stack>
#include <queue>
#include <map>
#include "Instance.h"

using namespace std;

class DynamicInstance : public Instance {
public:
    DynamicInstance(uint8_t n, int16_t ID);
    ~DynamicInstance();

    uint32_t sumaCen = 0;
    uint32_t** pole; //[n][c]


    vector<uint32_t> *poradnik; // uklada cislo dalšího prvku ke zpracování (radek, tedy cenu)
    vector<uint32_t> *DalsiPoradnik;

    void naalokujPole();
    uint64_t prohledej();


    //FPTAS
    unsigned long prohledej(double epsilon);
};


#endif //BATOH_DYNAMICINSTANCE_H
