//
// Created by Bagnat on 21.10.2019.
//

#ifndef BATOH_TIMEMEASURE_H
#define BATOH_TIMEMEASURE_H

#include <chrono>
#include <iostream>

std::chrono::steady_clock::time_point beginCustomVar, endCustomVar;

void timeStart(){
    beginCustomVar = std::chrono::steady_clock::now();
}
void timeStop(){
    endCustomVar = std::chrono::steady_clock::now();
}
void timePrintMicro(){
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(endCustomVar - beginCustomVar).count();
}

uint64_t timeGet(){
    return (uint64_t) std::chrono::duration_cast<std::chrono::microseconds>(endCustomVar - beginCustomVar).count();
}




#endif //BATOH_TIMEMEASURE_H
