//
// Created by Bagnat on 18.10.2019.
//

#ifndef BATOH_INSTANCEBRUTAL_H
#define BATOH_INSTANCEBRUTAL_H

#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
#include <cmath>
#include <stdint.h>
#include "Instance.h"

using namespace std;

class InstanceBrutal : public Instance{
public:
    InstanceBrutal(uint8_t n, int16_t ID, bool rozhodovaci, bool BB);

    bool rozhodovaci, BB;


    //napočítání si větví
    vector<uint32_t> vetve; //např. 3. index značí sumu 3 posledních itemů od konce seznamu itemů

    /**
     * @param pocetNull uint16_t počet nezafixovaných čísel odzadu
     * @return
     */
    bool UzitecnaCesta(const uint32_t aktualniCena, const uint16_t fixovaneBity);
    bool Splnuje(const uint32_t aktualniVaha, const uint32_t aktualniCena) const;
    bool Nadlimitni(const uint32_t aktualniVaha) const;



    friend ostream &operator<<(ostream &os, const InstanceBrutal &instance);

    uint64_t mereni = 0;
};


#endif //BATOH_INSTANCEBRUTAL_H
