#ifndef BATOH_GREEDYINSTANCE_H
#define BATOH_GREEDYINSTANCE_H

#include "Instance.h"

using namespace std;

class GreedyInstance : public Instance {
public:
    GreedyInstance(uint8_t n, int16_t ID);

    uint64_t citac = 0;
    multimap<double,uint8_t> mapa; //fitnes a ukazatel na item
    int32_t zbyvajiciVaha;


};


#endif //BATOH_GREEDYINSTANCE_H
