//
// Created by Bagnat on 18.10.2019.
//

#include <cstdlib>
#include <iostream>
#include <limits>
#include <fstream>
#include <cstring>
#include "Instance.h"


Instance::Instance(uint8_t n, int16_t ID) {

    //příprava souboru s daty, nalezení požadovaného řádku
    ifstream myfile;
    int16_t lineID;
    string name = SADA_DAT + to_string(n) + "_inst.dat";
    myfile.open( name );
    if (!myfile.is_open()) {
        cout << "soubor " << name << " nebyl nalezen -> konec" << endl;
        exit(1);
    }
    while( ! myfile.eof() )
    {
        myfile >> lineID;
        if(abs(ID) == abs(lineID) ) break; //abs kvuli zlomyslnym, kde není - u id
        myfile.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    if(myfile.eof()){
        myfile.close();
        cout << "v soubor " << name << " nebyl nalezen id " << ID << endl;
        exit(1);
    }

    myfile >> pocetVeci;
    myfile >> maximalniKapacita;
    if( strcmp("ZKC_unique/ZKC", SADA_DAT) != 0 ) myfile >> pozadovanaCena;

    uint32_t tmp1, tmp2;
    while(!myfile.eof() && myfile.peek() != '\n'){
        myfile >> tmp1 >> tmp2;
        pridatItem(tmp1, tmp2);
    }
    myfile.close();

}

void Instance::pridatItem(const uint32_t vaha, const uint32_t cena) {
    Itemy.emplace_back(vaha, cena);
}

void Instance::vypisConfig(){
    return vypisConfig(config);
}

void Instance::vypisConfig(vector<bool> config){
    uint32_t vaha = 0, cena = 0, cnt = 0;
    string str = "";
    for (auto i = config.begin(); i < config.end(); i++) {
        str += to_string((*i)) + " ";
        if( (*i) ){
            vaha += Itemy[cnt].first;
            cena += Itemy[cnt].second;
        }
        cnt++;
    }
    //cout << "(v="<<vaha<<",c="<<cena<<")" << endl;
    cout << cena << " " << str;
}

void Instance::vypis() {
    if (nejlepsiCena > 0) {
       vypisConfig();
    } else {
        for (int i = 0; i <= pocetVeci; ++i) {
            cout << "0";
            if (i != pocetVeci) cout << " ";
        }
    }
    cout << endl;
}

void Instance::Redux() {
    //nalezeni nejvetsi veci, co se vejde
    int cisloPrvku = -1;
    for (int i = 0; i < (int)Itemy.size(); ++i) {
        if(Itemy[i].first <= maximalniKapacita){
            if(cisloPrvku == -1 || Itemy[i].second > Itemy[cisloPrvku].second ) cisloPrvku = i;
        }
    }
    if(cisloPrvku == -1) return; //není nic, co by se do batohu vešlo

    if(nejlepsiCena < Itemy[cisloPrvku].second) {
        nejlepsiCena = Itemy[cisloPrvku].second;
        for (int i = 0; i < (int)Itemy.size(); ++i) config[i] = cisloPrvku == i;
    }

}