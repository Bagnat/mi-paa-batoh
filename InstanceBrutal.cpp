//
// Created by Bagnat on 18.10.2019.
//

#include "InstanceBrutal.h"

bool InstanceBrutal::UzitecnaCesta(const uint32_t aktualniCena, const uint16_t fixovaneBity) {
    return (aktualniCena+vetve[fixovaneBity]) > nejlepsiCena;
}

bool InstanceBrutal::Splnuje(const uint32_t aktualniVaha, const uint32_t aktualniCena) const {
    return aktualniVaha <= maximalniKapacita && aktualniVaha != 0;
}

bool InstanceBrutal::Nadlimitni(const uint32_t aktualniVaha) const {
    return aktualniVaha > maximalniKapacita;
}


ostream &operator<<(ostream &os, const InstanceBrutal &instance) {
    return os;
}



InstanceBrutal::InstanceBrutal(uint8_t n, int16_t ID, bool rozhodovaci, bool BB) : Instance(n, ID){

    this->rozhodovaci = rozhodovaci;
    this->BB = BB;
    if(!rozhodovaci) this->pozadovanaCena = 0; //hledani nejlepsiho

    //dopocitani vetvi
    for (uint i = 0; i < Itemy.size(); ++i) {
        vetve.push_back(0);
    }
    for (uint i = 0; i < Itemy.size(); ++i) {
        for (uint j = 0; j <= i; ++j) {
            vetve[j] += Itemy[i].second;
        }

    }
}


