#!/usr/bin/env bash

maxN=40
sleepForSec=600
cesta=./report/    
nazev=mereni

g++ -std=c++14 -Wall -o program main.cpp InstanceBrutal.cpp DynamicInstance.cpp GreedyInstance.cpp Instance.cpp

#if false; then
$(./program normal-rozhodovaci mereni $maxN - >${cesta}rozhodovaci_normal_$nazev & sleep $sleepForSec ; kill $!) &
$(./program BB-rozhodovaci mereni $maxN - >${cesta}rozhodovaci_BB_$nazev & sleep $sleepForSec ; kill $!) &
#fi

$(./program normal-konstruktivni mereni $maxN - >${cesta}konstruktivni_normal_$nazev & sleep $sleepForSec ; kill $!) &
$(./program BB-konstruktivni mereni $maxN - >${cesta}konstruktivni_BB_$nazev & sleep $sleepForSec ; kill $!) &
$(./program dynamicka mereni $maxN - >${cesta}konstruktivni_dynamicka_$nazev & sleep $sleepForSec ; kill $!) &


sleep $sleepForSec
sleep 3
rm program