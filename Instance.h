//
// Created by Bagnat on 18.10.2019.
//

#ifndef BATOH_INSTANCE_H
#define BATOH_INSTANCE_H
//#define SADA_DAT "ZKC_unique/ZKC"
#define SADA_DAT "NR/NR"
//#define SADA_DAT "instance/"

#include <cstdint>
#include <vector>

using namespace std;

class Instance {
public:

    uint32_t pozadovanaCena;
    uint32_t maximalniKapacita;
    uint16_t pocetVeci;
    vector<pair<uint32_t,uint32_t>> Itemy; //vaha, cena (v tomto pořadí)

    uint32_t nejlepsiCena = 0;
    vector<bool> config;    //nejlepší řešení

    Instance(uint8_t n, int16_t ID);
    void pridatItem(const uint32_t vaha, const uint32_t cena);

    void vypisConfig();
    void vypisConfig(vector<bool> config);
    void vypis();


    void Redux();
};


#endif //BATOH_INSTANCE_H
