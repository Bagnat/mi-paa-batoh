//
// Created by Bagnat on 18.10.2019.
//

#include <cstdlib>
#include <sys/param.h>
#include <iostream>
#include <cstring>
#include <cmath>
#include "DynamicInstance.h"
using namespace std;

DynamicInstance::DynamicInstance(uint8_t n, int16_t ID) : Instance(n, ID){

    sumaCen = 0;
    for (uint i = 0; i < Itemy.size(); ++i) {
        sumaCen += Itemy[i].second;
    }

}

void DynamicInstance::naalokujPole() {
    //alokace, sumCena musí být vypočtena
    pole = (uint32_t**) malloc((Itemy.size()+1) * sizeof(uint32_t*));
    for (uint j = 0; j <= Itemy.size(); ++j) {
        pole[j] = (uint32_t*) calloc(sumaCen+1, sizeof(uint32_t));
        if(pole[j] == nullptr) throw "nešlo naalokovat!";
        memset(pole[j], 255, (sumaCen+1)*4);
    }
}


DynamicInstance::~DynamicInstance() {
    if(pole != nullptr)
    {
        for (uint j = 0; j <= Itemy.size(); ++j) {
            free(pole[j]);
        }
        free(pole);
    }

}



uint64_t DynamicInstance::prohledej() {
    naalokujPole();

    vector<uint32_t>* tmp;
    poradnik = new vector<uint32_t>;
    DalsiPoradnik = new vector<uint32_t>;


    pole[0][0]=0;
    DalsiPoradnik->push_back(0);

    uint8_t cntN = 0;
    uint64_t citac = 0;
    while (!DalsiPoradnik->empty() && (uint8_t)(cntN) < Itemy.size()){
        tmp = poradnik;
        poradnik = DalsiPoradnik;
        DalsiPoradnik = tmp;

        while (!poradnik->empty()) {
            uint32_t cena = poradnik->back();
            poradnik->pop_back();

            //nepridani
            if (pole[cntN + 1][cena] > pole[cntN][cena])
            {
                pole[cntN + 1][cena] = pole[cntN][cena];
                DalsiPoradnik->push_back(cena);
            };

            //pridani
            if(pole[cntN][cena]+Itemy[cntN].first > maximalniKapacita) continue;
            if (pole[cntN + 1][cena+Itemy[cntN].second] > (pole[cntN][cena]+Itemy[cntN].first))
            {
                pole[cntN + 1][cena+Itemy[cntN].second] = (pole[cntN][cena]+Itemy[cntN].first);
                DalsiPoradnik->push_back(cena+Itemy[cntN].second);
            };

            citac++;
        }

        cntN++;
    }

    delete poradnik;
    delete DalsiPoradnik;
/*
    //vypis pole pro debug
    for (int l = sumaCen; l >= 0; --l) {
        bool vypsat = false;
        for (int k = 0; k < Itemy.size(); ++k) {
            if(pole[k][l] != (uint32_t)-1) vypsat = true;
        }
        if(vypsat)
        {
            cout << l;
            for (int k = 0; k < Itemy.size(); ++k) {
                if(pole[k][l] != (uint32_t)-1)
                    cout << "\t" << pole[k][l];
                else cout << "\t-1";
            }
            cout << "\t" <<endl;
        }
    }
*/


    //nalezeni nejlepsi ceny
    nejlepsiCena = 0;
    for (uint i = 0; i <= sumaCen; i++) {
        if(pole[Itemy.size()][i] <= maximalniKapacita) nejlepsiCena = i;
    }

    uint32_t cena = nejlepsiCena;
    config = vector<bool>(Itemy.size(), false);
    for (int j = Itemy.size(); j > 0 ; --j) {
        //cout << cena << endl;
        if(pole[j][cena] != pole[j-1][cena]){
            config[j-1] = true;
            cena -= Itemy[j-1].second;
        }
    }
    if(cena != 0 ) config[0] = true;

    return citac;
}

unsigned long DynamicInstance::prohledej(double epsilon) {
 /*   double tmp = (epsilon*(double)sumaCen)/(double)Itemy.size();
    tmp = log(tmp)/log(2);
    uint8_t b = floor(tmp);
    if(b==0) return;
*/

    double K = ((epsilon*(double)sumaCen)/(double)Itemy.size());

    vector<uint32_t> puvodniCeny;
    sumaCen = 0;
    for (uint i = 0; i < Itemy.size(); ++i) {
        puvodniCeny.push_back(Itemy[i].second);
        Itemy[i].second = (uint32_t) ceil(((double)Itemy[i].second)/K);
        sumaCen += Itemy[i].second;
    }

    uint64_t citac = prohledej();

    //vraceni cen a prepocitani
    nejlepsiCena = 0;
    for (uint j = 0; j < Itemy.size(); ++j) {
        Itemy[j].second = puvodniCeny[j];
        if(config[j]) nejlepsiCena += Itemy[j].second;
    }


    return citac;
}


