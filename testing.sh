

g++ -std=c++14 -Wall -o program main.cpp InstanceBrutal.cpp DynamicInstance.cpp GreedyInstance.cpp Instance.cpp

for n in 4 10 15 20 22 25 27 30 32 35 37 40
do

  #for mod in "normal-konstruktivni" "BB-konstruktivni" "dynamicka"
  for mod in "dynamicka"
  do
      echo
      echo  -------- $mod - $n -------------
      time ./program $mod kontrola $n >/tmp/t
      echo
      diff -w /tmp/t NR/NK${n}_sol.dat

      if test $? -eq 0
      then
          echo "zcela stejne"
      fi


  done

done