#include <iostream>
#include <stdint.h>

#include "BrutalForce.cpp"
#include "DynamicInstance.h"
#include "timeMeasure.h"
#include "GreedyInstance.h"
#include "Evoluce/Evoluce.h"


//definuje, zda chci vracet pocet instancí nebo cas
//#define POCET_KONFIGURACI true
#define ZDROJ_FILE false

using namespace std;

void help(){
    cout << "help" << endl <<
            "\tprogram TYP OUTPUT N [-]" << endl <<
            "\t\tTYP" << endl <<
            "\t\t\tnormal-rozhodovaci = rozhodovaci brutal force bez B&B" << endl <<
            "\t\t\tnormal-konstruktivni = konstruktivni brutal force bez B&B" << endl <<
            "\t\t\tBB-rozhodovaci = rozhodovaci brutal force s B&B" << endl <<
            "\t\t\tBB-konstruktivni = konstruktivni brutal force s B&B" << endl <<
            "\t\t\tdynamicka = dynamicke programovani" << endl <<
            "\t\tOUTPUT" << endl <<
            "\t\t\tmereni = vraci csv počet neměřených dat" << endl <<
            "\t\t\tkontrola = vraci data ve formatu NR/sol" << endl <<
            "\t\tN = n, které se ma provádět, 'n -' provede vsechny <= n" << endl << endl <<
            "\tprogram heuristika HEURISTIKA N" << endl <<
            "\t\tHEURITIKA" << endl <<
            "\t\t\tgreedy = jednoduchá greedy heuristika" << endl <<
            "\t\t\tredux = modifikovana greedy heuritika (na konci porovnat s největsim prvkem)" << endl <<
            "\t\tN = n, které se ma provádět, 'n -' provede vsechny <= n" << endl << endl <<
            "\tprogram fptas EPSILON N" << endl <<
            "\t\tEPSILON = relativni chyba v procentech!" << endl <<
            "\t\tN = n, které se má provádět" << endl << endl <<
            "\t\tprogram evoluce - N" << endl <<
            "pozn. kontrola vstupu neni soucasti" << endl;
}


int main(int argc, char *argv[]) {
    if(argc != 4 && argc != 5){
        help();
        return 1;
    }


    uint maxN, n[] = {4, 10, 15, 20, 22, 25, 27, 30, 32, 35, 37, 40, 10000};
    for (int k = 0; n[k] <= stoi(argv[3]); ++k) {
        maxN = k;
    }

    if(string(argv[1]) == "normal-rozhodovaci" || string(argv[1]) == "normal-konstruktivni" || string(argv[1]) == "BB-rozhodovaci" || string(argv[1]) == "BB-konstruktivni"){
        //jeste nebylo programovano modularne
        bool BB = string(argv[1]) == "BB-rozhodovaci" || string(argv[1]) == "BB-konstruktivni";
        bool rozhodovaci = string(argv[1]) == "normal-rozhodovaci" || string(argv[1]) == "BB-rozhodovaci";

        std::cout.setf(std::ios::unitbuf);

        if(string(argv[2]) == "mereni")
        {
            //vypsat počet navštívených instanci
                for (int j = (argc == 4 ? maxN : 0); j <= maxN; ++j) {
                    for (int16_t i = -1; i >=-100; --i) {

                        //vrací podle nastavení v POCET_KONFIGURACI
                        uint64_t tmp = runBrutal(n[j], i, false, rozhodovaci, BB);

                        if (i != -1) cout << ",";
                        cout << tmp;
                    }
                    cout << endl;
                }
        }else{
            //vypsat výsledky
            for (int j = (argc == 4 ? maxN : 0); j <= maxN; ++j) {
                for (int16_t i = -1; i >=-100; --i) {
                    runBrutal(n[j], i, true, rozhodovaci, BB);

                }
            }
        }
    }else if(string(argv[1]) == "dynamicka"){
        int16_t ttmmpp = 0;
        if(string(argv[2]) == "valgrind") ttmmpp = -3;
        if(string(argv[2]) == "mereni") ttmmpp = -100;
        if(string(argv[2]) == "kontrola") ttmmpp = -500;

        for (int j = (argc == 4 ? maxN : 0); j <= maxN; ++j) {
            for (int16_t i = -1; i >= ttmmpp; --i) {

                timeStart();
                DynamicInstance batoh( n[j] , i);
                uint64_t tmp = batoh.prohledej();
                timeStop();

                cerr << batoh.nejlepsiCena << endl;
                if(string(argv[2]) == "mereni"){
                    if(i != -1) cout << ",";
                    if(POCET_KONFIGURACI) cout << tmp;
                    else timePrintMicro();
                }else if(string(argv[2]) == "kontrola"){
                    cout << fabs(i) << " " << batoh.pocetVeci << " ";
                    batoh.vypis();
                }

            }
            cout << endl;
        }

    }else if(string(argv[1]) == "heuristika"){

        for (int16_t i = -1; i >= -100; --i) {

            timeStart();
            GreedyInstance batoh( n[maxN] , i);
            if(string(argv[2]) == "redux") batoh.Redux();
            timeStop();

            cerr << batoh.nejlepsiCena;
            if(POCET_KONFIGURACI) cout << batoh.citac;
            else{
                cout << timeGet();
            }

            if(i==-100) {
                /*cout << endl;
                cerr << endl;*/
            }
            else {
                cout << ",";
                cerr << ",";
            }
        }



    }else if(string(argv[1]) == "fptas"){

        for (int j = (argc == 4 ? maxN : 0); j <= maxN; ++j) {
            for (int16_t i = -1; i >= -100; --i) {

                timeStart();
                DynamicInstance batoh( n[j] , i);
                uint64_t tmp = batoh.prohledej( stod(argv[2])/100 );
                batoh.Redux();
                timeStop();

                if(POCET_KONFIGURACI) {
                    cout << tmp;
                }
                else{
                    cout << timeGet();
                }

                cerr << batoh.nejlepsiCena;
                if(i!=-100) {
                    cout << ",";
                    cerr << ",";
                }


            }
            //cout << endl;
        }


    }else if(string(argv[1]) == "evoluce"){
        double rozdily = 0;
        for(int ins = -1; ins>=-100; ins--){

            Instance instance( stoi(argv[3]),ins);
            Evoluce<JedinecBatoh> evoluce( &instance );

            uint32_t uvaznuti = 0, nejlepsiReseni = 0;
            int i = 0;
            for (;; ++i) {
                evoluce.Zij();

                //evoluce.PrintStatus();
                //evoluce.PrintHistogramFitness();

                //určuje konec -> po 100 nevylepsení = konec
                if(instance.nejlepsiCena != nejlepsiReseni){
                    nejlepsiReseni = instance.nejlepsiCena;
                    uvaznuti=0;
                }else if(uvaznuti == POCET_POKUSU_PRED_UVAZNUTIM) {
                    break;
                };
                uvaznuti++;
                //evoluce.PrintPopulation();
                //cout << ",";
                if(i >= POCET_POKUSU_CELKEM) break;
            }
            //return 1;
            cout << i << ") " << instance.nejlepsiCena << endl;

            //best (kontrola)
            DynamicInstance batoh( stoi(argv[3]),ins);
            batoh.prohledej();
            cout << batoh.nejlepsiCena << endl;

            rozdily += (double)instance.nejlepsiCena / (double)batoh.nejlepsiCena;
        }
        /*rozdily /= 100;
        cout << "pomer: " << rozdily << endl;*/

    }else{
        help();
        return 1;
    }

    return 0;
}

