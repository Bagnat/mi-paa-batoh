# PARAMETRY:
# 1 = n
# 2 = m
# 3 = W
# 4 = w
# 5 = C
# 6 = c
# 7 = k
# 8 = "normal" "BB" "dynamicka" "fptas"
# 9 = epsilon u fptas
# 10 = type "max" "avg" "list" "vysledky" "chyba" 
#           (list - v�stup programu neupravuje a spust� jednu instanci, kterou 100 x permutuje) 
#           (vysledky - je list, ale m�sto casi/konfig vrac� napoc�tan� hodnoty)
#           (chyba - ur�eno pouze pro fptas a greedy - vrac� pole dvojic, dvojici tvo�� v�sledek aproximativn�ho alg a v�sledek optim�ln�ho, pole jde p�es 100 r�zn�ch instanc�)
# 11 = konfigurace ("yes"|"no") - yes vrac� po�et navst�ven�ch konf., no vrac� �as v�po�tu
 
# smazani souboru obsahujici nenalezen: grep "n" ./data -R | cut -d: -f1 | xargs rm

generator=../gen/kg2
permutator=../gen/kg_perm
pwd=$PWD
dataFolder="./data"
instanceFolder="./instance"
pocetInstanci=100
dtbGet="bash ./fast-bash-script-database/searchKeyMatch.sh"
dtbGive="bash ./fast-bash-script-database/appendToFile.sh"

if [ ${11} == "yes" ]; then loc_program="program"; else loc_program="program_cas"; fi
program="../${loc_program}"

#pro debug
if test 1 -eq 2
then
  echo "$0 $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11}"
  echo $program
  exit 0
fi

#rm $program $dataFolder/* $instanceFolder/*

if test ! -f "$generator"; then
    echo "neni generator"
    exit 1
fi

if test ! -f "$permutator"; then
    echo "neni permutator"
    exit 1
fi

if test ! -f "$program"; then
    cd ..
    
    if test ${11} == "yes"; then def="-DPOCET_KONFIGURACI=true"; else def="-DPOCET_KONFIGURACI=false"; fi
    
    g++ -std=c++14 "$def" -Wall -o $loc_program main.cpp InstanceBrutal.cpp DynamicInstance.cpp GreedyInstance.cpp Instance.cpp
    cd $pwd
fi


fileName="$1_$2_$3_$4_$5_$6_$7_$8_$9_${10}_${11}"



docastny="$instanceFolder/${1}_inst.dat"   #z toho si to vezme program
trvaly="$instanceFolder/$fileName"


#existuje
vysledek="$($dtbGet "$fileName")"

if test ! -z "$vysledek"
then
    if [ "list" == "${10}" ] || [ "vysledky" == "${10}" ]; then vysledek="$(echo "$vysledek" | tr " " "," )"; fi
    if [ "chyba" == "${10}" ]; then vysledek="$(echo "$vysledek" | tr "-" "," )"; fi
    echo "$vysledek"
    exit 0
fi
 

#vygenerovani testu
$generator -I 1 -n "$1" -N "$pocetInstanci" -m "$2" -W "$3" -w "$4" -C "$5" -c "$6" -k "$7" >"$docastny"

#permutace
if [ ${10} == "list" ] || [ ${10} == "vysledky" ]
then
     head -n 1 <"$docastny" | ../gen/kg_perm -d 1 -N "$pocetInstanci" | cut -d" " -f2-1500 | nl >"${docastny}_tmp"
     mv "${docastny}_tmp" "$docastny"
fi

        
case $8 in
    "normal")
        param="normal-konstruktivni mereni $1"
        ;;
    
    "BB")
        param="BB-konstruktivni mereni $1"
        ;;
        
    "greedy")
        param="heuristika greedy $1"
        ;;
        
    "dynamicka")
        param="dynamicka mereni $1"
        ;;
        
    "fptas")
        param="fptas $9 $1"
        ;;
esac

 
timeout 10 $program $param >"$dataFolder/$fileName" 2>"$dataFolder/${fileName}_vysledky"

if test $? == "124" 
then
    $dtbGive "$fileName" 0 
    echo 0
    exit 1 
fi


      

if test "avg" == "${10}"
then    
    #avg   
    varavg=$(awk -v pocetInstanci="$pocetInstanci" '{print "(" $1 ")/" pocetInstanci }' <"$dataFolder/$fileName"  | tr , + | bc )
    $dtbGive "$fileName" "$varavg"
    echo "$varavg"  
    
elif test "max" == "${10}"
then

    #max
    varmax=$(tr , "\n" <"$dataFolder/$fileName" | sort -r -n | head -n 1)
    $dtbGive "$fileName" "$varmax" 
    echo "$varmax" 
    
elif test "list" == "${10}"
then

    #list
    varlist="{$(cat "$dataFolder/$fileName")}"
    $dtbGive "$fileName" "$varlist" 
    echo "$varlist" 
    
elif test "vysledky" == "${10}"
then

    #vysledky
    varlist="{$(cat "$dataFolder/${fileName}_vysledky")}"
    $dtbGive "$fileName" "$varlist" 
    echo "$varlist" 

elif test "chyba" == "${10}"
then

    #chyba
    #spoc�t�n� opravdov�ho vysledku
    timeout 60 $program dynamicka mereni "$1" >/dev/null 2>"$dataFolder/${fileName}_vysledky_optimal"
    vysledky=$(tr "," "\n" <"$dataFolder/${fileName}_vysledky")
    optimal=$(tr "," "\n" <"$dataFolder/${fileName}_vysledky_optimal")
    ulozit=$(paste <(printf "%s" "$vysledky") <(printf "%s" "$optimal") | awk '{ print "{" $1 "-" $2 "}" }' | tr "\n" "-" | sed 's/-$//' | awk '{ print "{" $0 "}" }')
   
    $dtbGive "$fileName" "$ulozit" 
    
    echo "$ulozit" | tr "-" ","
    
else

    exit 100
    
fi

exit 0


# smazani ur�it�ch dat v content:  sed -i '/normal/Q' content.csv