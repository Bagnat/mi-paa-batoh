#include <map>
#include "GreedyInstance.h"

GreedyInstance::GreedyInstance(uint8_t n, int16_t ID) : Instance(n, ID){
    for (uint i = 0; i < Itemy.size(); ++i) {
        uint32_t vaha = Itemy[i].first;
        uint32_t cena = Itemy[i].second;
        double fitness = (double)vaha/(double)cena;
        mapa.insert({fitness, i});
    }

    config = vector<bool>(Itemy.size(), false);
    zbyvajiciVaha = maximalniKapacita;
    nejlepsiCena = 0;
    for (auto it = mapa.begin(); it != mapa.end() && zbyvajiciVaha >= 0; it++) {
       citac ++;
       auto item = Itemy[ (*it).second ];
       if( item.first <= (uint32_t)zbyvajiciVaha){
           zbyvajiciVaha -= item.first;
           nejlepsiCena += item.second;
           config[ (*it).second  ] = true;
       }
    }
}



